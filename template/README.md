# {{site_name}}

Lecture notes and teaching material used for the Delft University of Technology course awesome course.

The compiled materials are available at https://{{cookiecutter.SERVER_URL}}

# Origin and technical support

This repository is based on a template for publishing lecture notes, developed
by Anton Akhmerov, who also hosts such repositories for other courses.
